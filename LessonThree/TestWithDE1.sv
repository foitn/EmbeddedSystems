module TestWithDE1(CLOCK_27[0], SW, HEX0, HEX1, HEX2, HEX3, VGA_R, VGA_B, VGA_G);
	input CLOCK_27[0];
	input [2:0] SW;
	output [6:0]HEX0, HEX1, HEX2, HEX3;
	output [3:0]VGA_R, VGA_G, VGA_B;
	reg [3:0] seg1, seg2, seg3, seg4;
	reg [23:0] clock;
	
	
	always @ (posedge CLOCK_27[0])
	begin
		clock <= clock+1;
		if(clock == 5400000)
			begin
				clock <= 0;
				if(SW[2])
					begin
						seg1 <= 0;
						seg2 <= 0;
						seg3 <= 0;
						seg4 <= 0;
					end
			else if(SW[1])
				if(SW[0]) 
					begin 
						seg1 <= seg1+1;
						if(seg1 == 9)
							begin
								seg1 <= 0;
								seg2 <= seg2+1;
								if(seg2 == 9)
									begin
										seg2 <= 0;
										seg3 <=seg3+1;
										if(seg3 == 9)
											begin
												seg3 <= 0;
												seg4 <= seg4+1;
												if(seg4 == 9)
													begin
														seg4 <= 0;
													end
											end
									end
							end
					end 
				else 
					begin 
						seg1 <= seg1-1;
						if(seg1 == 0)
							begin
								seg1 <= 9;
								seg2 <= seg2-1;
								if(seg2 == 0)
									begin
										seg2 <= 9;
										seg3 <= seg3-1;
										if(seg3 == 0)
											begin
												seg3 <= 9;
												seg4 <= seg4-1;
												if(seg4 == 0)
													begin
														seg4 <= 9;
													end
											end
									end
							end
					end
			end
	end
	
	assign HEX0=
    (seg1==0) ? 7'b1000000:
    (seg1==1) ? 7'b1111001:
    (seg1==2) ? 7'b0100100:
    (seg1==3) ? 7'b0110000:
    (seg1==4) ? 7'b0011001:
    (seg1==5) ? 7'b0010010:
    (seg1==6) ? 7'b0000010:
    (seg1==7) ? 7'b1111000:
    (seg1==8) ? 7'b0000000:
					 7'b0010000;
					 	
	assign HEX1=
    (seg2==0) ? 7'b1000000:
    (seg2==1) ? 7'b1111001:
    (seg2==2) ? 7'b0100100:
    (seg2==3) ? 7'b0110000:
    (seg2==4) ? 7'b0011001:
    (seg2==5) ? 7'b0010010:
    (seg2==6) ? 7'b0000010:
    (seg2==7) ? 7'b1111000:
    (seg2==8) ? 7'b0000000:
					 7'b0010000;
					 	
	assign HEX2=
    (seg3==0) ? 7'b1000000:
    (seg3==1) ? 7'b1111001:
    (seg3==2) ? 7'b0100100:
    (seg3==3) ? 7'b0110000:
    (seg3==4) ? 7'b0011001:
    (seg3==5) ? 7'b0010010:
    (seg3==6) ? 7'b0000010:
    (seg3==7) ? 7'b1111000:
    (seg3==8) ? 7'b0000000:
					 7'b0010000;
					 	
	assign HEX3=
    (seg4==0) ? 7'b1000000:
    (seg4==1) ? 7'b1111001:
    (seg4==2) ? 7'b0100100:
    (seg4==3) ? 7'b0110000:
    (seg4==4) ? 7'b0011001:
    (seg4==5) ? 7'b0010010:
    (seg4==6) ? 7'b0000010:
    (seg4==7) ? 7'b1111000:
    (seg4==8) ? 7'b0000000:
					 7'b0010000;
	
endmodule