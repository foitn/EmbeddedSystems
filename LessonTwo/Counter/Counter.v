module Counter(clk, s0, s1, s2, segm7);
	input clk, s0, s1, s2;
	output [6:0] segm7;
	reg  [3:0] cts;
	
	
	always @ (posedge clk)
	if(s2)
		begin
			cts <= 0;
		end
	else if(s1)
		if(s0) 
			begin 
				cts <= cts+1;
				if(cts == 9)
					//begin
						cts <= 0;
					//end
			end 
		else 
			begin 
				cts <= cts-1;
				if(cts == 0)
					//begin
						cts <= 9;
					//end
			end
	assign segm7=
    (cts==0) ? 7'b1000000:
    (cts==1) ? 7'b1111001:
    (cts==2) ? 7'b0100100:
    (cts==3) ? 7'b0110000:
    (cts==4) ? 7'b0011001:
    (cts==5) ? 7'b0010010:
    (cts==6) ? 7'b0000010:
    (cts==7) ? 7'b1111000:
    (cts==8) ? 7'b0000000:
					7'b0010000;
endmodule