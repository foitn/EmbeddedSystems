module	VGA_Pattern	(	//	Read Out Side
						oRed,
						oGreen,
						oBlue,
						iVGA_X,
						iVGA_Y,
						iVGA_CLK,
						playerOne,
						playerTwo,
						currentPlayer,
						//	Control Signals
						iRST_N	);
//	Read Out Side
output	reg	[9:0]	oRed;
output	reg	[9:0]	oGreen;
output	reg	[9:0]	oBlue;
input	[9:0]		iVGA_X;
input	[9:0]		iVGA_Y;
input				iVGA_CLK;
input [8:0]		playerOne;
input [8:0]		playerTwo;
input				currentPlayer;

//	Control Signals
input				iRST_N;

always@(posedge iVGA_CLK or negedge iRST_N)
begin
	if(!iRST_N)
	begin
		oRed		<=	0;
		oGreen	<=	0;
		oBlue		<=	0;
	end
	else
	begin
		oRed 		<= //(iVGA_X == playerOne)															?	0:
						((iVGA_X >= 500 && iVGA_X <= 515) && (iVGA_Y == 25 || iVGA_Y == 40))?	1023:
						((iVGA_Y >= 25 && iVGA_Y <= 55) && (iVGA_X == 500 || iVGA_X == 520 || iVGA_X == 535 || iVGA_X == 540 || iVGA_X == 553 || iVGA_X == 565 || iVGA_X == 580))?	1023:
						((iVGA_X >= 520 && iVGA_X <= 535) && (iVGA_Y == 25 || iVGA_Y == 55))?	1023:
						((iVGA_X >= 546 && iVGA_X <= 560) && (iVGA_Y == 25))					?	1023:
						(iVGA_X == 565 && iVGA_Y == 26)												?  1023:
						(iVGA_X == 566 && iVGA_Y == 27)												?  1023:
						(iVGA_X == 566 && iVGA_Y == 28)												?  1023:
						(iVGA_X == 567 && iVGA_Y == 29)												?  1023:
						(iVGA_X == 567 && iVGA_Y == 30)												?  1023:
						(iVGA_X == 568 && iVGA_Y == 31)												?  1023:
						(iVGA_X == 568 && iVGA_Y == 32)												?  1023:
						(iVGA_X == 569 && iVGA_Y == 33)												?  1023:
						(iVGA_X == 569 && iVGA_Y == 34)												?  1023:
						(iVGA_X == 570 && iVGA_Y == 35)												?  1023:
						(iVGA_X == 570 && iVGA_Y == 36)												?  1023:
						(iVGA_X == 571 && iVGA_Y == 37)												?  1023:
						(iVGA_X == 571 && iVGA_Y == 38)												?  1023:
						(iVGA_X == 572 && iVGA_Y == 39)												?  1023:
						(iVGA_X == 572 && iVGA_Y == 40)												?  1023:
						(iVGA_X == 573 && iVGA_Y == 41)												?  1023:
						(iVGA_X == 573 && iVGA_Y == 42)												?  1023:
						(iVGA_X == 574 && iVGA_Y == 43)												?  1023:
						(iVGA_X == 574 && iVGA_Y == 44)												?  1023:
						(iVGA_X == 575 && iVGA_Y == 45)												?  1023:
						(iVGA_X == 575 && iVGA_Y == 46)												?  1023:
						(iVGA_X == 576 && iVGA_Y == 47)												?  1023:
						(iVGA_X == 576 && iVGA_Y == 48)												?  1023:
						(iVGA_X == 577 && iVGA_Y == 49)												?  1023:
						(iVGA_X == 577 && iVGA_Y == 50)												?  1023:
						(iVGA_X == 578 && iVGA_Y == 51)												?  1023:
						(iVGA_X == 579 && iVGA_Y == 52)												?  1023:
						(iVGA_X == 579 && iVGA_Y == 53)												?  1023:
						(iVGA_X == 580 && iVGA_Y == 54)												?  1023:		
						
						((currentPlayer==0)&&(iVGA_X > 520 && iVGA_X < 535) && (iVGA_Y > 25 && iVGA_Y < 55))? 1023:
						(iVGA_X > 475 || iVGA_X < 25 || iVGA_Y > 475 || iVGA_Y < 25)		? 	0:
						(iVGA_Y == 25 || iVGA_Y == 175 || iVGA_Y == 325 || iVGA_Y == 475)	? 	1023:
						(iVGA_X == 25 || iVGA_X == 175 || iVGA_X == 325 || iVGA_X == 475)	? 	1023:
						(playerOne[((3*((iVGA_Y-25)/150))+(iVGA_X-25)/150)] == 1)			?	700:
						
																													0;
		oGreen 	<= 
						((iVGA_X >= 500 && iVGA_X <= 515) && (iVGA_Y == 25 || iVGA_Y == 40))?	1023:
						((iVGA_Y >= 25 && iVGA_Y <= 55) && (iVGA_X == 500 || iVGA_X == 520 || iVGA_X == 535 || iVGA_X == 540 || iVGA_X == 553 || iVGA_X == 565 || iVGA_X == 580))?	1023:
						((iVGA_X >= 520 && iVGA_X <= 535) && (iVGA_Y == 25 || iVGA_Y == 55))?	1023:
						((iVGA_X >= 546 && iVGA_X <= 560) && (iVGA_Y == 25))					?	1023:
												(iVGA_X == 565 && iVGA_Y == 26)												?  1023:
						(iVGA_X == 566 && iVGA_Y == 27)												?  1023:
						(iVGA_X == 566 && iVGA_Y == 28)												?  1023:
						(iVGA_X == 567 && iVGA_Y == 29)												?  1023:
						(iVGA_X == 567 && iVGA_Y == 30)												?  1023:
						(iVGA_X == 568 && iVGA_Y == 31)												?  1023:
						(iVGA_X == 568 && iVGA_Y == 32)												?  1023:
						(iVGA_X == 569 && iVGA_Y == 33)												?  1023:
						(iVGA_X == 569 && iVGA_Y == 34)												?  1023:
						(iVGA_X == 570 && iVGA_Y == 35)												?  1023:
						(iVGA_X == 570 && iVGA_Y == 36)												?  1023:
						(iVGA_X == 571 && iVGA_Y == 37)												?  1023:
						(iVGA_X == 571 && iVGA_Y == 38)												?  1023:
						(iVGA_X == 572 && iVGA_Y == 39)												?  1023:
						(iVGA_X == 572 && iVGA_Y == 40)												?  1023:
						(iVGA_X == 573 && iVGA_Y == 41)												?  1023:
						(iVGA_X == 573 && iVGA_Y == 42)												?  1023:
						(iVGA_X == 574 && iVGA_Y == 43)												?  1023:
						(iVGA_X == 574 && iVGA_Y == 44)												?  1023:
						(iVGA_X == 575 && iVGA_Y == 45)												?  1023:
						(iVGA_X == 575 && iVGA_Y == 46)												?  1023:
						(iVGA_X == 576 && iVGA_Y == 47)												?  1023:
						(iVGA_X == 576 && iVGA_Y == 48)												?  1023:
						(iVGA_X == 577 && iVGA_Y == 49)												?  1023:
						(iVGA_X == 577 && iVGA_Y == 50)												?  1023:
						(iVGA_X == 578 && iVGA_Y == 51)												?  1023:
						(iVGA_X == 579 && iVGA_Y == 52)												?  1023:
						(iVGA_X == 579 && iVGA_Y == 53)												?  1023:
						(iVGA_X == 580 && iVGA_Y == 54)												?  1023:		
		
		
						(iVGA_X > 475 || iVGA_X < 25 || iVGA_Y > 475 || iVGA_Y < 25)		? 	0:
						(iVGA_Y == 25 || iVGA_Y == 175 || iVGA_Y == 325 || iVGA_Y == 475)	? 	1023:
						(iVGA_X == 25 || iVGA_X == 175 || iVGA_X == 325 || iVGA_X == 475)	? 	1023:
																													0;
																													
		oBlue 	<= 
						((iVGA_X >= 500 && iVGA_X <= 515) && (iVGA_Y == 25 || iVGA_Y == 40))?	1023:
						((iVGA_Y >= 25 && iVGA_Y <= 55) && (iVGA_X == 500 || iVGA_X == 520 || iVGA_X == 535 || iVGA_X == 540 || iVGA_X == 553 || iVGA_X == 565 || iVGA_X == 580))?	1023:
						((iVGA_X >= 520 && iVGA_X <= 535) && (iVGA_Y == 25 || iVGA_Y == 55))?	1023:
						((iVGA_X >= 546 && iVGA_X <= 560) && (iVGA_Y == 25))					?	1023:
												(iVGA_X == 565 && iVGA_Y == 26)												?  1023:
						(iVGA_X == 566 && iVGA_Y == 27)												?  1023:
						(iVGA_X == 566 && iVGA_Y == 28)												?  1023:
						(iVGA_X == 567 && iVGA_Y == 29)												?  1023:
						(iVGA_X == 567 && iVGA_Y == 30)												?  1023:
						(iVGA_X == 568 && iVGA_Y == 31)												?  1023:
						(iVGA_X == 568 && iVGA_Y == 32)												?  1023:
						(iVGA_X == 569 && iVGA_Y == 33)												?  1023:
						(iVGA_X == 569 && iVGA_Y == 34)												?  1023:
						(iVGA_X == 570 && iVGA_Y == 35)												?  1023:
						(iVGA_X == 570 && iVGA_Y == 36)												?  1023:
						(iVGA_X == 571 && iVGA_Y == 37)												?  1023:
						(iVGA_X == 571 && iVGA_Y == 38)												?  1023:
						(iVGA_X == 572 && iVGA_Y == 39)												?  1023:
						(iVGA_X == 572 && iVGA_Y == 40)												?  1023:
						(iVGA_X == 573 && iVGA_Y == 41)												?  1023:
						(iVGA_X == 573 && iVGA_Y == 42)												?  1023:
						(iVGA_X == 574 && iVGA_Y == 43)												?  1023:
						(iVGA_X == 574 && iVGA_Y == 44)												?  1023:
						(iVGA_X == 575 && iVGA_Y == 45)												?  1023:
						(iVGA_X == 575 && iVGA_Y == 46)												?  1023:
						(iVGA_X == 576 && iVGA_Y == 47)												?  1023:
						(iVGA_X == 576 && iVGA_Y == 48)												?  1023:
						(iVGA_X == 577 && iVGA_Y == 49)												?  1023:
						(iVGA_X == 577 && iVGA_Y == 50)												?  1023:
						(iVGA_X == 578 && iVGA_Y == 51)												?  1023:
						(iVGA_X == 579 && iVGA_Y == 52)												?  1023:
						(iVGA_X == 579 && iVGA_Y == 53)												?  1023:
						(iVGA_X == 580 && iVGA_Y == 54)												?  1023:		
						
						((currentPlayer==1)&&(iVGA_X > 520 && iVGA_X < 535) && (iVGA_Y > 25 && iVGA_Y < 55))? 1023:
						(iVGA_X > 475 || iVGA_X < 25 || iVGA_Y > 475 || iVGA_Y < 25)		? 	0:
						(iVGA_Y == 25 || iVGA_Y == 175 || iVGA_Y == 325 || iVGA_Y == 475)	? 	1023:
						(iVGA_X == 25 || iVGA_X == 175 || iVGA_X == 325 || iVGA_X == 475)	? 	1023:
						(playerTwo[((3*((iVGA_Y-25)/150))+(iVGA_X-25)/150)] == 1)			?	600:
						
																													0;
	end
end

endmodule