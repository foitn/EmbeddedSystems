module DE1_Default
	(
		////////////////////	Clock Input	 	////////////////////	 
		CLOCK_24,						//	24 MHz
		CLOCK_27,						//	27 MHz
		CLOCK_50,						//	50 MHz
		EXT_CLOCK,						//	External Clock
		////////////////////	Push Button		////////////////////
		KEY,							//	Pushbutton[3:0]
		////////////////////	DPDT Switch		////////////////////
		SW,								//	Toggle Switch[9:0]
		////////////////////////	UART	////////////////////////
		UART_TXD,						//	UART Transmitter
		UART_RXD,						//	UART Receiver
		////////////////////	VGA		////////////////////////////
		VGA_HS,							//	VGA H_SYNC
		VGA_VS,							//	VGA V_SYNC
		VGA_R,   						//	VGA Red[3:0]
		VGA_G,	 						//	VGA Green[3:0]
		VGA_B,  							//	VGA Blue[3:0]
	);

////////////////////////	Clock Input	 	////////////////////////
input	[1:0]		CLOCK_24;				//	24 MHz
input	[1:0]		CLOCK_27;				//	27 MHz
input				CLOCK_50;				//	50 MHz
input				EXT_CLOCK;				//	External Clock
////////////////////////	Push Button		////////////////////////
input	[3:0]		KEY;					//	Pushbutton[3:0]
////////////////////////	DPDT Switch		////////////////////////
input	[9:0]		SW;						//	Toggle Switch[9:0]

////////////////////////////	UART	////////////////////////////
output			UART_TXD;				//	UART Transmitter
input				UART_RXD;				//	UART Receiver

////////////////////////	VGA			////////////////////////////
output			VGA_HS;					//	VGA H_SYNC
output			VGA_VS;					//	VGA V_SYNC
output	[3:0]	VGA_R;   				//	VGA Red[3:0]
output	[3:0]	VGA_G;	 				//	VGA Green[3:0]
output	[3:0]	VGA_B;   				//	VGA Blue[3:0]
wire				VGA_CTRL_CLK;
wire				AUD_CTRL_CLK;
wire	[9:0]		mVGA_X;
wire	[9:0]		mVGA_Y;
wire	[9:0]		mVGA_R;
wire	[9:0]		mVGA_G;
wire	[9:0]		mVGA_B;
wire	[9:0]		mPAR_R;
wire	[9:0]		mPAR_G;
wire	[9:0]		mPAR_B;
wire	[9:0]		mOSD_R;
wire	[9:0]		mOSD_G;
wire	[9:0]		mOSD_B;
wire	[9:0]		oVGA_R;
wire	[9:0]		oVGA_G;
wire	[9:0]		oVGA_B;
wire	[19:0]	mVGA_ADDR;
reg	[27:0]	Cont;
reg	[9:0]		mLEDR;
reg				ST;
wire	[7:0]	uart_data;

////////////////////////	OWN		////////////////////////////
reg	[8:0]		playerOne;
reg 	[8:0]		playerTwo;
reg	[8:0]		recentValue;
reg	[1:0]		datareceiving;
reg	[4:0]    counter;
reg   [1:0]		currentPlayer;
reg	[13:0]	clockCounter;

initial playerOne = 9'b000000000;
initial playerTwo = 9'b000000000;
initial datareceiving = 0;
initial currentPlayer = 0;
initial clockCounter = 0;

always@(posedge CLOCK_50)
begin
	clockCounter <= clockCounter + 1;
	if(clockCounter == 5208)
	begin
		if(datareceiving == 0)
		begin
			if(UART_RXD == 0)
			begin
				datareceiving <= 1;
			end
		end
		else
		begin
			if(counter < 8)
			begin
				recentValue[counter] <= UART_RXD;
				counter <= counter +1;
			end
			else if(counter > 10)
			begin
				if(recentValue == 253)
				begin
					playerOne <= 0;
					playerTwo <= 0;
				end
				else
				begin
					if(currentPlayer == 0)
					begin
						playerOne[9-recentValue] <= 1;
						currentPlayer <= 1;
					end
					else
					begin
						playerTwo[9-recentValue] <= 1;
						currentPlayer <= 0;
					end
				end
				counter <= 0;
				datareceiving <= 0;
				recentValue <=0;
			end
			else
			begin
				counter <= counter +1;
			end
		end
		
		if(UART_RXD == 0 && datareceiving == 0)
		begin
			datareceiving <= 1;
		end	
		clockCounter <= 0; 
	end
end
//	VGA Data 10-bit to 4-bit
assign	VGA_R		=	oVGA_R[9:6];
assign	VGA_G		=	oVGA_G[9:6];
assign	VGA_B		=	oVGA_B[9:6];
//	VGA Source Select
assign	mVGA_R		=	mPAR_R ;
assign	mVGA_G		=	mPAR_G ;
assign	mVGA_B		=	mPAR_B ;



VGA_Audio_PLL 		u3	(	.inclk0(CLOCK_27[0]),.c0(VGA_CTRL_CLK),.c1(AUD_CTRL_CLK)	);

VGA_Controller		u4	(	//	Host Side
							.iCursor_RGB_EN(4'h7),	
							.oAddress(mVGA_ADDR),					
							.oCoord_X(mVGA_X),
							.oCoord_Y(mVGA_Y),
							.iRed(mVGA_R),
							.iGreen(mVGA_G),
							.iBlue(mVGA_B),
							//	VGA Side
							.oVGA_R(oVGA_R),
							.oVGA_G(oVGA_G),
							.oVGA_B(oVGA_B),
							.oVGA_H_SYNC(VGA_HS),
							.oVGA_V_SYNC(VGA_VS),
							//	Control Signal
							.iCLK(VGA_CTRL_CLK),
							.iRST_N(KEY[0])	);

VGA_Pattern			u5	(	//	Read Out Side
							.oRed(mPAR_R),
							.oGreen(mPAR_G),
							.oBlue(mPAR_B),
							.iVGA_X(mVGA_X),
							.iVGA_Y(mVGA_Y),
							.iVGA_CLK(VGA_CTRL_CLK),
							.playerOne(playerOne),
							.playerTwo(playerTwo),
							.currentPlayer(currentPlayer),
							//	Control Signals
							.iRST_N(KEY[0])	);

  uart_0 the_uart_0
    (
      .address       (uart_0_s1_address),
      .begintransfer (uart_0_s1_begintransfer),
      .chipselect    (uart_0_s1_chipselect),
      .clk           (clk),
      .dataavailable (uart_0_s1_dataavailable),
      .irq           (uart_0_s1_irq),
      .read_n        (uart_0_s1_read_n),
      .readdata      (uart_data),
      .readyfordata  (uart_0_s1_readyfordata),
      .reset_n       (uart_0_s1_reset_n),
      .rxd           (UART_RXD),
      .txd           (UART_TXD),
      .write_n       (uart_0_s1_write_n),
      .writedata     (uart_0_s1_writedata)
    );

endmodule
