module Counter(clk, s0, s1, s2, leds);
	input clk, s0, s1, s2;
	output [3:0] leds;
	reg [3:0] cts;
 

	always @ (posedge clk)
	if(s2)
		begin
			cts <= 0;
		end
	else if(s1)
		if(s0) 
			begin 
				cts <= cts+1;
			end 
		else 
			begin 
				cts <= cts-1;
			end
	assign leds=cts;
endmodule