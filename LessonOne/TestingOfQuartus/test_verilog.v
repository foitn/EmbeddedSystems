module TestOne (switch, led0, led1, led2);
  input switch;
  output led0, led1, led2;
  assign led1=switch;
  assign led0=~switch;
  assign led2=~switch;
endmodule  